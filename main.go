package main

import (
	"fmt"
	"net/http"
	"strings"
)

func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Method)
	if r.Method == "GET" {
		var homepage string = `
			<html>
				<head>
					<title>Shorten Link</title>
					<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
				
				</head>
				<body>
					<div class="container">
						<h2>Simplify your link</h2>
						<form action="/" method="post">
							<div class="input-group mb-3">
								<input name="originalUrl" type="text" class="form-control" placeholder="Your original URL here" aria-label="Your original URL here" aria-describedby="button-addon2">
								<div class="input-group-append">
									<input class="btn btn-outline-success" type="submit" id="button-addon2" value="SHORTEN URL">
								</div>
							</div>
						</form>
					</div>

					<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
					<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
				</body>
			</html>
			`

		fmt.Fprintf(w, homepage)
	} else {
		r.ParseForm()
		url := r.PostForm.Get("originalUrl")
		if !strings.Contains(url, "http://") && !strings.Contains(url, "https://") {
			fmt.Println("-----1---")
			if strings.Index(url, "http://") != 0 && strings.Index(url, "https://") != 0 {
				fmt.Println("-----2---")
				newUrl := "https://"
				newUrl += url
				res, err := http.Get(newUrl)
				if err == nil {
					fmt.Println(res.StatusCode)
					if res.StatusCode == 200 {
						fmt.Fprintf(w, "OK")
					} else {
						fmt.Fprintf(w, "FAIL")
					}
				}
			}
		} else {
			res, err := http.Get(url)
			if err == nil {
				fmt.Println(res.StatusCode)
				if res.StatusCode == 200 {
					fmt.Fprintf(w, "OK")
				} else {
					fmt.Fprintf(w, "FAIL")
				}
			}
		}
	}
}

func main() {
	http.HandleFunc("/", homeHandler)

	http.ListenAndServe(":6969", nil)
}
